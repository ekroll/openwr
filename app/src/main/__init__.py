import pathlib

def here():
    return (pathlib
        .Path(__file__)
        .parent)