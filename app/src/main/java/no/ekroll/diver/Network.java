package no.ekroll.diver;


import android.os.Build;
import android.app.PendingIntent;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import androidx.core.app.RemoteInput;
import androidx.core.app.NotificationCompat;


public class Network {
    private Context context;
    private NotificationManager notificationManager;
    private final String NOTIFICATION_CHANNEL;
    private final String NOTIFICATION_REPLY_KEY;

    public Network(Context context, String notification_channel, String reply_key) {
        this.context = context;
        this.NOTIFICATION_CHANNEL = notification_channel;
        this.NOTIFICATION_REPLY_KEY = reply_key;
    }

    private void createNotificationChannel(String channel_id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new 
                NotificationChannel(
                    channel_id,
                    channel_id,
                    NotificationManager.IMPORTANCE_DEFAULT
                );

            notificationManager = context.getSystemService(
                NotificationManager.class
            );

            notificationManager.createNotificationChannel(channel);
        }
    }

    private NotificationCompat.Action createNotificationAction(
        int icon,
        String placeholder_text,
        Context context,
        int flag
    ) {
        RemoteInput remoteInput = new 
            RemoteInput.Builder(
                NOTIFICATION_REPLY_KEY
            )
            .setLabel(placeholder_text)
            .build();

        PendingIntent replyIntent =
            PendingIntent.getBroadcast(
                context,
                0,
                new Intent(context, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT
            );

        NotificationCompat.Action action =
            new NotificationCompat.Action.Builder(
                icon,
                placeholder_text,
                replyIntent
            )
            .addRemoteInput(remoteInput)
            .build();

        return action;
    }

    public void promptServerAddress() {
        Log.d("bleh", "server address prompt");
        createNotificationChannel(NOTIFICATION_CHANNEL);
        NotificationCompat.Action action = createNotificationAction(
            R.drawable.icon,
            "ip_address:port",
            context,
            PendingIntent.FLAG_UPDATE_CURRENT
        );

        NotificationCompat.Builder builder = new 
            NotificationCompat.Builder(
                context, NOTIFICATION_CHANNEL
            )
            .setSmallIcon(R.drawable.icon)
            .setContentTitle("Enter server address:")
            .addAction(action)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        notificationManager.notify(0, builder.build());
    }
}
