package no.ekroll.diver;


import android.os.Bundle;
import android.app.Activity;
import android.app.RemoteInput;
import android.util.Log;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.widget.TextView;


public class MainActivity extends Activity {
    public class Sniffer extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("BroadcastReceiver", "Received some broadcast");
            Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
            TextView tv = (TextView)findViewById(R.id.textView);

            if (remoteInput != null) {
                String m_serverAddress = (String) remoteInput.getCharSequence("server_address");
                tv.setText(m_serverAddress);
            }

            else {
                tv.setText("remoteInput was null");
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Sniffer sniffer = new Sniffer();
        Network network = new
            Network(
                this,"diver",
                "server_address"
            );

        network.promptServerAddress();
    }
}