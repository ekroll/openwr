import std/[
  strformat,
  logging
]

import res as r
import wwr/log
import ddd

proc main() =
  try:
    let url = r.getEndpoint("kartverket_raster_capabilities")
    let text = r.downstream(url)
    let status = r.tagExists(text, "Title")
    debug(&"tag title exists: {status}")

  except:
    fatal("could not retreive map info")
    quit(-1)
  
  let frame = ddd.getFrame()
  var frametime = 1.0 / frame.fps.float
  var running = ddd.start(frame)

  var suzanne: Object = r.getObject("suzanne")
  ddd.addObject(suzanne)

  while running:
    let framestart = times.cpuTime()

    sleep(1)
    running = ddd.render(frametime)

    frametime = framestart - cpuTime()
  ddd.finish()


when isMainModule:
  main()

