import std/[
  strformat,
  logging,
  os
]

import res as r

let 
  dir = r.getPath("log")
  errLog = r.get_logfile("errors")
  runLog = r.get_logfile("runtime")

if not os.dirExists(dir):
  try:
    createDir(dir)

    if not os.fileExists(runLog):
      try:
        open(runLog, fmWrite).close()
      except:
        echo &"failed to make {runLog}"
      
    if not os.fileExists(errLog):
      try:
        open(errLog, fmWrite).close()
      except:
        echo &"failed to make {errLog}"

  except:
    echo &"Failed to make {dir}"

let
  fmt = "The $date at $time in [$appname]: $levelname = "
  consoleLogger = logging.newConsoleLogger(
    fmtStr = fmt
  )

  runLogger = logging.newRollingFileLogger(
    filename = runLog,
    fmtStr = fmt
  )

  errLogger = logging.newFileLogger(
    levelThreshold = lvlError,
    filename = errLog,
    fmtStr = fmt
  )

logging.addHandler(consoleLogger)
logging.addHandler(runLogger)
logging.addHandler(errLogger)

echo "testing logger:"
debug("harmless test message")
info("harmless test message")
notice("harmless test message")
warn("harmless test message")
error("harmless test message")
fatal("harmless test message")
