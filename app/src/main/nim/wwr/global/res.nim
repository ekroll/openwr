import std/[
  httpclient,
  strformat,
  strscans,
  strutils,
  parsexml,
  logging,
  streams,
  json,
  os
]

import res/model
import kashae
import ddd


proc getJson*(path: string): JsonNode {.cache.} =
  ## Used when parsing JSON manually
  debug(path)
  let content = readFile(path)
  let json = parseJson(content)
  doAssert json.kind == JObject
  return json


proc getPath*(key: string): string {.cache.} =
  ## Returns the path to the logging directory in client.json
  let cfg = get_json("./config/paths.json")
  let path = cfg{key}.getStr()
  return path


proc getLogfile*(filename: string): string =
  ## Returns path to new/old logfile by name
  let dir = getPath("log")
  return dir & filename & ".log"


proc getEndpoint*(key: string): string {.cache.} =
  ## Returns url to endpoint specified in remode_endpoints.json
  let cfg = get_json("./config/endpoints.json")
  let url = cfg{key}.getStr()
  return url


proc downstream*(url: string): string =
  ## "Downloads" to memory for quick processing
  var client = newHttpClient()

  try:
    client.getContent(url)
    
  except:
    "failed"


proc tagExists*(text: string, tag: string): bool {.cache.} =
  ## Checks if a tag exists in a XML document
  var s = newStringStream(text)

  if s == nil:
    quit("could not make stream")

  var x: XmlParser
  open(x, s, "nofile")

  while true:
    x.next()

    case x.kind 
    of xmlElementStart:
      if cmpIgnoreCase(x.elementName, "title") == 0:
        info(fmt"x.elementName == {x.elementName}")
        return true

    of xmlEof:
      return false

    else: break

  x.close()
  false


proc parseModel*(path: string): (ddd.Model, int) =
  try:
    let (_, _, ext) = os.splitFile(path)
    case ext:
      of "obj":
        return model.obj(path)

      #[
      of "gltf":
        return gltf(path)
      ]#

      else:
        notice(&"unsupported model filetype: {ext}")

  except:
    error(&"failed to parse {path}")
