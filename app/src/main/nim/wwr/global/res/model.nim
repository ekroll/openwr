import ddd

proc obj(path: string): (ddd.Model, int) =
  var model: ddd.Model
  var vc: int = 0
  var idx: int = 0

  for line in lines path:
    var vertex: ddd.Vertex
    var x, y, z: float

    if line.scanf(
      "v $f $f $f",
      x, y, z
    ):
      vertex.position = [
        x.float32,
        y.float32,
        z.float32
      ]
      vc += 1
    
    elif line.scanf(
      "vt $f $f",
      x, y
    ):
      vertex.texcoord = [
        x.float32, 
        y.float32
      ]

    elif line.scanf(
      "vn $f $f $f",
      x, y, z
    ):
      vertex.normal = [
        x.float32,
        y.float32,
        z.float32
      ]
    
    else:
      notice(&"unsupported obj stuff in {path}:{idx} {line}")

    model.vertices.add(vertex)
    idx += 1
  
  (model, vc)
