def run(cfg):
    os.chdir(cfg.here() / "wws")
    cfg.die(["mix", "docgen"])
    cfg.die(["mix", "phx.server"])
    os.chdir(cfg.project_root())


if not __name__ == "__main__":
    import subprocess
    import pathlib
    import sys
    import os
else:
    print("Why are you running?")
