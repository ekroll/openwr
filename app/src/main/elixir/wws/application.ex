defmodule WWS.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # And https://hexdocs.pm/elixir/Supervisor.html
  # for more information on OTP and Supervisors
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      WWS.Repo, # Start the Ecto repository
      WWW.Telemetry, # Start the Telemetry supervisor
      {Phoenix.PubSub, name: WWS.PubSub},# Start the PubSub system
      WWW.Endpoint # Start the Endpoint (http/https)
    ]

    opts = [strategy: :one_for_one, name: WWS.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    WWW.Endpoint.config_change(changed, removed)
    :ok
  end
end
