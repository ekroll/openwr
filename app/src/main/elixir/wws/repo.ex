defmodule WWS.Repo do
  use Ecto.Repo,
    otp_app: :wws,
    adapter: Ecto.Adapters.Postgres
end
