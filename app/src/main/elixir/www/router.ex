defmodule WWW.Router do
  use WWW, :router

  import WWW.UserAuth
  import Phoenix.LiveDashboard.Router

  @moduledoc """
    Routes all requests passed on from endpoint.ex
    to the different parts of the application.
  """

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", WWW do
    pipe_through :api
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {WWW.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
    plug WWW.Plugs.Locale, "en"
  end

  scope "/", WWW do
    pipe_through [:browser]
    get "/", PageController, :index
    get "/test", TestController, :index
    get "/test/:message", TestController, :show
    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
    post "/users/confirm/:token", UserConfirmationController, :update
  end

  scope "/", WWW do
    pipe_through [:browser, :require_authenticated_user]
    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
  end

  scope "/", WWW do
    pipe_through [:browser, :redirect_if_user_is_authenticated]
    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/admin" do
    pipe_through [:browser, :require_authenticated_user]
    live_dashboard "/dashboard", metrics: WWW.Telemetry
  end

  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
