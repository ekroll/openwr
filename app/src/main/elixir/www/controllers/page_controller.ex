defmodule WWW.PageController do
  use WWW, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
