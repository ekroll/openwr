defmodule WWW.TestController do
  use WWW, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def show(conn, %{"message" => message}) do
    render(conn, "show.html",
      host: inspect(conn.host),
      method: inspect(conn.method),
      headers: inspect(conn.req_headers, pretty: true),
      message: inspect(message)
    )
  end
end
