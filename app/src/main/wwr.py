def build(cfg):
    print(__file__)
    os.chdir(cfg.here() / "wwr")
    cmd = ["nimble", "build"]
    subprocess.run(cmd)
    os.chdir(cfg.project_root())


def run(cfg):
    os.chdir(cfg.here() / "wwr")
    cmd = ["nimble", "run"]
    subprocess.run(cmd)
    os.chdir(cfg.project_root())


if not __name__ == "__main__":
    import subprocess
    import pathlib
    import sys
    import os
else:
    print("Why are you running?")
