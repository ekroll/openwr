import "phoenix_html"
import "../css/www.css"
import {Socket} from "phoenix"
import topbar from "./vendor/topbar"
import {LiveSocket} from "phoenix_live_view"


let csrfToken = document
    .querySelector("meta[name='csrf-token']")
    .getAttribute("content")

let liveSocket = new LiveSocket("/live", Socket, {
    params: {_csrf_token: csrfToken}
})

topbar.config({barColors: {0: "#29d"}, shadowColor: "rgba(0, 0, 0, .3)"})
window.addEventListener("phx:page-loading-start", info => topbar.show())
window.addEventListener("phx:page-loading-stop", info => topbar.hide())
liveSocket.connect()


// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket


var map = L.map('map', {zoomControl: false}).setView([0, 0], 13)
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© OpenStreetMap'
}).addTo(map);

L.marker([62.535907, 6.711866]).addTo(map)
    .bindPopup('Verdens Navle')
    .openPopup()

L.marker([62.4669941, 6.3500559]).addTo(map)
    .bindPopup('Sodom')
    .openPopup()


L.marker([62.4660222, 6.3564235]).addTo(map)
    .bindPopup('Gomorrah')
    .openPopup()


// Show context menu
document.addEventListener("contextmenu", (e) => {
    e.preventDefault()
    console.log(e)
    if (!showMenu) {
        contextMenu = document.getElementById("context-menu")
        const { clientX: mx, clientY: my } = e
        contextMenu.style.top = `${my}px`
        contextMenu.style.left = `${mx}px`
        contextMenu.classList.add("visible")
    }
}, false)


// Hide context menu
document.body.addEventListener("click", (e) => {
    console.log(e)
    contextMenu = document
        .getElementById("context-menu")

    if (e.target.offsetParent != contextMenu) {
        contextMenu.classList.remove("visible")
    }
}, false)


// Toggle menu
showMenu = true
function toggleMenu() {
    showMenu = !showMenu
    menu = document.getElementById("menu")
    map = document.getElementById("map")

    if (showMenu)  {
        menu.classList.remove("invisible")
        map.classList.remove("noblur")
    }   else   {
        menu.classList.add("invisible")
        map.classList.add("noblur")
    }
}
 

// Simple site-wide keyboard navigation
ctrlDown = false
document.addEventListener("keydown", (e) => {
    ctrlDown = e.ctrlKey
    console.log(e.key)

    if (e.altKey) {
        switch(e.key) {
            case "f": toggleFullscreen()
            case "h": window.location = "/"; break
            case "j": window.location = "/users/register"; break
            case "k": window.location = "/users/log_in"; break
        }
    }

    else {
        switch(e.key) {
            case "Escape": toggleMenu()
        }
    }

}, false)


// Zoom override
document.addEventListener("wheel", (e) => {
    if (ctrlDown) {
        e.preventDefault()
    }
}, false)



