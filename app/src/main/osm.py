import subprocess


def _tile_path(cfg, tile):
    data = cfg.get_path("static")
    data / f"{tile}.pbf"
    return data.resolve()
    

def setup(cfg):
    colormap = cfg.get_map("color")
    
    path = _tile_path(cfg, tile)
    if not path.isfile():
        cfg.Download(tile).to(path)
    
    volume = cfg.get_volume("osm")
    volumes = subprocess.check_output([
        "docker", "volume", "ls"
    ])
        
    if volume not in volumes:
        subprocess.run([
            "docker", "volume", "create", volume
        ])


def run(cfg):
    psql_version = cfg.get_version("psql") # 12
    server_version = cfg.get_version("tile-server") # 1.3.10

    subprocess.run([
        "docker", "run", 
        "-v", f"{path}:/data.osc.pbf",
        "-v", f"openstreetmap-data:/var/lib/postgresql/{psql_version}/main",
        f"overv/openstreetmap-tile-server:{server_version}", "import"
    ])
