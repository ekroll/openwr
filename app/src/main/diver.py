def build(config):
    gradlew = config.get_gradlew()
    cmd = [gradlew, "assembleDebug"]
    if subprocess.call(cmd, shell=True) != 0: sys.exit()


def get_package(config):
    os.chdir(pathlib.Path(__file__).parent)
    tree = ET.parse("AndroidManifest.xml")
    os.chdir(config.project_root())
    manifest = tree.getroot()
    package = manifest.attrib["package"]
    print(package)
    return package


def install(config):
    package = get_package(config)
    cmd = [
        "adb", "-d", "install", "-r", 
        f"app/build/outputs/apk/{package}-debug.apk"
    ]

    if subprocess.call(cmd, shell=True) != 0: sys.exit()


def run(config):
    package = get_package(config)
    cmd = [
        "adb", "shell", "monkey",
        "-p", package, "-c",
        "android.intent.category.LAUNCHER", 1
    ]

    if subprocess.call(cmd, shell=True) != 0: sys.exit()


if not __name__ == "__main__":
    import subprocess
    import pathlib
    import sys
    import xml.etree.ElementTree as ET
    import os
else:
    print("Why are you running?")