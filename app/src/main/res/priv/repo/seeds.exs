# Script for populating the database. You can run it as:
#
#     mix run app/src/main/res/priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     WWS.Repo.insert!(%WWS.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
