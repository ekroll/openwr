import unittest
import cfg

test "Parse JSON from file":
  check cfg.get_json() == "stuff"


test "Get Endpoint from file":
  check cfg.get_endpoint() == "stuff"


test "Get path to logger directory":
  check cfg.get_logdir() == "stuff"


test "Get path to logfile":
  check cfg.get_logfile() == "stuff"


