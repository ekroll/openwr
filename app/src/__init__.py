import importlib

def do(action, target, test, cfg):
    pack = ".test" if test else ".main"
    module = importlib.import_module(
        f".{target}", package=__name__ + pack
    )
    
    action = getattr(module, action)
    action(cfg)
