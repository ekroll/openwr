FROM alpine
RUN apk update && apk upgrade
RUN apk add python3 elixir nimble wget nim git
RUN git clone https://gitlab.com/ekroll/openwr
RUN wget https://download.geofabrik.de/africa/zambia-latest.osm.pbf

WORKDIR openwr
RUN python3 openwr.py --setup


