# Open World Renderer (not even alpha)
![alt text](./app/src/main/res/drawable/banner.png "Title") \
The purpose of this project is to learn a bunch of stuff.
Will probably not become anything, but I'll rehearse full-stack and graphics stuff.
It is centered around exploring the parts of the world that are publicly available.
This includes a map website that can (should) spawn a 3D application at that location.


## Subprojects
The [repository](https://gitlab.com/ekroll/openwip) consists of the following:
* `wws` The website backend. (GPLv3 licensed)
* `www` The website frontend, part of the former. (GPLv3 licensed)
* `wwr` The native client linked to in frontend & probably a 
    [AUR](https://aur.archlinux.org/) package in the future. (GPLv3 licensed)
* `ddd` The 3D renderer used by wwr. 
    A nimble [package](https://gitlab.com/ekroll/ddd) included as a git submodule. 
    (MIT licensed)
* `mod` for embedding scripting languages.
    A nimble [package](https://gitlab.com/ekroll/mod) included as a git submodule. 
    (MIT licensed)
* `Diver` APK aimed at making you phone work as a VR HMD. (GPLv3 licensed)


## External Resources and API's:
* [LearnOpenGL](https://learnopengl.com/)
* [TheCherno](https://www.youtube.com/user/TheChernoProject) on 
[modern OpenGL](https://www.youtube.com/watch?v=W3gAzLwfIP0&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2).
* [thebennybox](https://www.youtube.com/user/thebennybox/featured) on 
[modern OpenGL](https://www.youtube.com/watch?v=ftiKrP3gW3k&list=PLEETnX-uPtBXT9T-hD0Bj31DSnwio-ywh).
* [The Art of Code](https://www.youtube.com/channel/UCcAlTqd9zID6aNX3TzwxJXg) on
shader programming.
* OpenAL [guide](https://github.com/kcat/openal-soft/wiki/Guide:-Introduction)
* [ThinMatrix](https://www.youtube.com/user/ThinMatrix) on 
[OpenAL](https://www.youtube.com/watch?v=BR8KjNkYURk&list=PLRIWtICgwaX2VNpAFjAZdlQw2pA1-5kU8)
* Embedding / Extending [Python](https://docs.python.org/3/extending/embedding.html)
* Embedding JavaScript with [QuickJS](https://github.com/bellard/quickjs/blob/master/examples)
* Phoenix [deployment guides](https://hexdocs.pm/phoenix/deployment.html)
* [Better HTML](https://chiamakaikeanyi.dev/writing-standards-compliant-html/)
* [Better CSS](https://chiamakaikeanyi.dev/writing-better-css-in-a-codebase/)
* [Kartverket](https://kartverket.no/api-og-data/terrengdata) (The Norwegian Mapping Authority)
* [OpenStreetMap](https://download.openstreetmap.fr/extracts/) (Frontend raster maps)
* Switch2OSM [Serving Tiles](https://switch2osm.org/serving-tiles/)
* [GeoServer Docs](https://docs.geoserver.org/latest/en/user/services/wms/reference.html)
* Kartverket's [example clients](https://github.com/kartverket/example-clients)
* Custom protocol handlers for
[Linux](https://unix.stackexchange.com/questions/497146/create-a-custom-url-protocol-handler),
[Windows](https://stackoverflow.com/questions/3057576/how-to-launch-an-application-from-a-browser) and
[OSX](https://itectec.com/superuser/how-to-configure-custom-url-handlers-on-os-x/)
* How to [pip install](https://www.activestate.com/resources/quick-reads/pip-install-git/) from git repo


## Development setup for Archlinux
This will set up a instance accessible at `https://localhost:2003` \
(documentation hosted locally on the website)
```bash
# Download and install everything:
sudo pacman -S inotify-tools postgres elixir git
git clone git@gitlab.com:ekroll/openwr.git
git subtree add -p openwr/app/src/main/nim/ddd git@gitlab.com:ekroll/ddd.git dev

# install "choosenim" from AUR your preferred way
choosenim stable


# Then set up PostgreSQL:
su - postgres -c "initdb --locale en_US.UTF-8 -D '/var/lib/postgres/data'"
sudo systemctl enable postgresql.service
sudo systemctl start postgresql.service

# Set up same credentials as your normal user here:
sudo su - postgres -c "createuser --interactive"

# Then create database as your normal user:
createdb openwr

# Temporary hack if the above does not work:
sudo mkdir -p /run/postgresql
sudo mkdir -p /var/postgresql/data
sudo chown postgres /var/postgres/data
sudo chown -R postgres:postgres /run/postgresql

cd openwr

# Update credentials in app/config/wws/dev.exs to match db
openwr -s # Run setup
openwr -h # Explore
```

## Development setup for Windows
WSL

## Development setup for Mac
Check out [this](https://wiki.archlinux.org/title/installation_guide)
guide for more information.
