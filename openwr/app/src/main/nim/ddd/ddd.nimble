# Package
version       = "0.1.0"
author        = "Elias Flåte Ekroll"
description   = "3D renderer ;)"
license       = "MIT"
srcDir        = "src/"


# Dependencies
requires "nim >= 1.4.8"
requires "openal >= 0.1.0"
requires "nimgl >= 1.3.2"
requires "glm >= 1.1.1"
requires "noisy >= 0.4.4"
#requires "futhark == 0.5.0" # Waiting until it builds relatively easily


# Testing
task test, "Runs the test suite":
  exec "nim c -r test/test.nim"

task docgen, "Generates documentation":
  exec "nim doc --project --index:on --outdir:doc"
