import std/[strformat, json]

import types as t
import logging
import kashae

import ddd/[
  continuum,
  matter,
  frame
]

export t


proc parseFrame*(path: string): t.Frame =
  ## Returns render configuration from json file
  readFile(path).parseJson().to(t.Frame)


proc parseObject*(name: string): t.DObject {.cache.} =
  ## Returns Object by name
  readfile(path).parseJson().to(t.DObject)


proc start*(cc: t.Frame): bool =
  ## Set up OpenGL and create a window
  debug("starting ddd renderer")
  frame.open(cc)


proc addObject*(obj: var t.DObject) =
  ## Breaks energy conservation
  debug(&"add {obj}")
  matter.create(obj)
  continuum.create(obj)


proc delObject*(obj: t.DObject) =
  debug(&"del {obj}")
  continuum.destroy(obj)
  matter.destroy(obj)


proc render*(delta: float): bool =
  ## Render one frame and output
  if frame.isOpen():
    frame.closeShutter()
    
    let status = frame.getStatus()
    let scenario = continuum.progress(delta)
    for obj in scenario:
      matter.update(obj, status)

    frame.openShutter(delta)
  frame.isActive()


proc finish*() =
  ## Clean up
  let objects = continuum.dump()
  for _object in objects:
    delObject(_object)

  frame.close()
