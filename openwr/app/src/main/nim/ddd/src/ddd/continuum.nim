import types as t
import logging

var continuum: seq[t.DObject]

proc dump(): seq[t.DObject] =
  debug(continuum)
  continuum


proc create*(obj: var t.DObject) =
  ## Adds a Object to the simulation
  continuum.add(obj)
  let id = len(continuum) - 1
  obj.id = some(id)


proc destroy*(obj: t.DObject) =
  ## Removes a ddd_object from the simulation
  continuum.delete(obj.id.get)


proc progress*(): seq[t.DObject] =
  ## Calculate next frame's continuum
  dump()
