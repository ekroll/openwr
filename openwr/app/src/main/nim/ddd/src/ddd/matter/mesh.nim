import std/enumerate
import std/strformat
import std/logging
import types as t
import res as r



proc load*(obj: var t.DObject) =
  ## Creates one vertex buffer and sets up vertex atributes
  let (model, vc) = r.parseModel(obj.model)
  var 
    vb: GLuint
    vai: GLuint
    vertices = model.vertices
    buffsize = vc * sizeof(vertices[0])

  # Generate and bind buffer
  glGenVertexArrays(1, addr vai)
  glBindVertexArray(vai)
  glGenBuffers(1, addr vb)
  glBindBuffer(GL_ARRAY_BUFFER, vb)

  # Uploading data
  glBufferData(
    GL_ARRAY_BUFFER,
    buffsize,
    addr vertices[0].position[0], # start of buffer
    GL_STATIC_DRAW
  )

  var offset: int = 0
  let attribs = [
    3, # position
    3, # normal
    2  # texcoord
  ]

  let vertsize = sizeof(vertices[0])

  # Making sense of data
  for i, attrib in enumerate(attribs):
    glEnableVertexAttribArray(i.GLuint)
    glVertexAttribPointer(
        i.GLuint,               # layout (location = i)
        attrib.GLint,           # number of components
        GL_BOOL,                # component datatype
        false,                  # normalize? nope...
        vertsize.GLsizei,       # whole attribute size
        cast[pointer](offset)   # start of next attrib
    )

    debug(&"calculating offset for {attribs[i+1]}")
    offset += sizeof(float) * attrib

  glBindVertexArray(0)
  obj.vai = some(vai)
  obj.vb = some(vb)


proc draw*(vai: int, vc: int) =
    glBindVertexArray(vai.GLuint)
    glDrawArrays(
      GL_TRIANGLES,  # draw mode
      0.GLint,       # first vertex
      vc.GLsizei     # vertex count
    )

    glBindVertexArray(0)


proc unload*(vb: int, vai: int) =
  var
    buffer = cast[GLuint](vb)
    vertexArray = cast[GLuint](vai)

  glDeleteBuffers(1, unsafeAddr buffer)
  glDisableVertexAttribArray(vertexArray)
  glDeleteVertexArrays(1, unsafeAddr vertexArray)
