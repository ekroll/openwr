import nimgl/[glfw, opengl]
import std/strformat
import std/logging
import types

# Callbacks

proc onOpenGLMessage (
  source: GLEnum,
  typ: GLEnum,
  id: GLuint,
  severity: GLEnum,
  length: GLsizei,
  message: ptr GLchar,
  userParam: pointer
) {.stdcall.} =
  if id.int in [131169, 131185, 131218, 131204]:
    return

  var err: string
  case typ
    of GL_DEBUG_TYPE_ERROR:               err = "error"
    of GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: err = "deprecated behaviour"
    of GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  err = "undefined behaviour"
    of GL_DEBUG_TYPE_PORTABILITY:         err = "portability"
    of GL_DEBUG_TYPE_PERFORMANCE:         err = "performance"
    of GL_DEBUG_TYPE_MARKER:              err = "marker"
    of GL_DEBUG_TYPE_PUSH_GROUP:          err = "push group"
    of GL_DEBUG_TYPE_POP_GROUP:           err = "pop group"
    of GL_DEBUG_TYPE_OTHER:               err = "other"
    else:                                 err = "unknown"

  var sev: string
  case severity
    of GL_DEBUG_SEVERITY_NOTIFICATION:    sev = "notification"
    of GL_DEBUG_SEVERITY_HIGH:            sev = "high"
    of GL_DEBUG_SEVERITY_MEDIUM:          sev = "medium"
    of GL_DEBUG_SEVERITY_LOW:             sev = "low"
    else:                                 sev = "unknown"

  var src: string
  case source
    of GL_DEBUG_SOURCE_API:               src = "api"
    of GL_DEBUG_SOURCE_WINDOW_SYSTEM:     src = "window system"
    of GL_DEBUG_SOURCE_SHADER_COMPILER:   src = "shader compiler"
    of GL_DEBUG_SOURCE_THIRD_PARTY:       src = "third-party"
    of GL_DEBUG_SOURCE_APPLICATION:       src = "application"
    of GL_DEBUG_SOURCE_OTHER:             src = "other"
    else:                                 err = "unknown"

  info(&"opengl debug message:\n\tid: {id}\n\ttype: {err}\n\tseverity{sev}\n\tsource: {src}\n\tmessage: {message}")


proc onWindowResized (
  window: GLFWWindow,
  width: int32,
  height: int32
) {.cdecl.} =
  info("window resized")


proc onFramebufferResized (
  window: GLFWwindow,
  width: GLint,
  height: GLint
) {.cdecl.} =
  glViewport(0, 0, width, height)


proc onDPIScaled (
  window: GLFWWindow,
  xscale: float32,
  yscale: float32
) {.cdecl.} =
  info("dpi changed")


proc onKeyPressed (
  window: GLFWWindow,
  key: int32,
  scancode: int32,
  action: int32,
  mods: int32
) {.cdecl.} =
  if key == GLFWKey.END and action == GLFWPress:
      window.setWindowShouldClose(true)


# Methods

proc open*(config: types.Config): bool =
  if not glfwInit():
    return false

  glfwWindowHint(GLFWOpenGLProfile, GLFW_OPENGL_COMPAT_PROFILE)
  glfwWindowHint(GLFWOpenGLForwardCompat, GLFW_TRUE)
  glfwWindowHint(GLFWOpenGLDebugContext, GLFW_TRUE)
  glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GLFW_FALSE)
  glfwWindowHint(GLFWRefreshRate, GLFW_DONT_CARE)
  glfwWindowHint(GLFWDoublebuffer, GLFW_TRUE)
  glfwWindowHint(GLFWContextVersionMajor, 4)
  glfwWindowHint(GLFWContextVersionMinor, 3)
  glfwWindowHint(GLFW_DECORATED, GLFW_TRUE)
  glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE)
  glfwWindowHint(GLFWResizable, GLFW_TRUE)
  glfwWindowHint(GLFWDepthBits, 16)
  glfwWindowHint(GLFWAlphaBits, 8)
  glfwWindowHint(GLFWGreenBits, 8)
  glfwWindowHint(GLFWBlueBits, 8)
  glfwWindowHint(GLFWredBits, 8)

  info("client: ", config)
  assert config.res[0] != 0
  assert config.res[1] != 0
  window = glfwCreateWindow(
    config.res[0],
    config.res[1],
    config.name.cstring,
    nil, nil
  )

  if window == nil:
    glfwTerminate()
    return false

  info("successfully constructed native window")
  discard window.setWindowSizeCallback(on_window_resized)
  discard window.setFramebufferSizeCallback(on_framebuffer_resized)
  discard window.setWindowContentScaleCallback(on_dpi_scaled)
  discard window.setKeyCallback(on_key_pressed)
  window.makeContextCurrent()
  glfwSwapInterval(0) # Disables Vsync

  try:
    info("attempting to load opengl context")
    discard glInit()
    info("successfully loaded opengl context:")
    info("opengl version: ", cast[cstring](glGetString(GL_VERSION)))
    info("opengl renderer: ", cast[cstring](glGetString(GL_RENDERER)))

  except:
    let
      e = getCurrentException()
      msg = getCurrentExceptionMsg()
      
    info(&"got exception {repr(e)}: {msg}")
    window.destroyWindow()
    return false

  var flags: GLint
  var zero_anger: GLuint = 0
  info("enabling opengl debug output")
  glGetIntegerv(GL_CONTEXT_FLAGS, addr flags)
  if (flags and GL_CONTEXT_FLAG_DEBUG_BIT.GLint) != 0:
    glEnable(GL_DEBUG_OUTPUT)
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS)
    glDebugMessageCallback(on_opengl_message, nil)
    glDebugMessageControl(
      GL_DONT_CARE,
      GL_DONT_CARE,
      GL_DONT_CARE,
      (GLsizei)(0),
      addr zero_anger,
      (GLboolean)(true)
    )
  
  else:
    info("failed enabling debug output")

  glEnable(GL_DEPTH_TEST)
  glEnable(GL_CULL_FACE)
  glCullFace(GL_BACK)
  true


proc openShutter*() =
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
  glClearColor(0.0, 0.0, 0.0, 1.0)


proc closeShutter*() =
  window.swapBuffers()
  glfwPollEvents()


proc isOpen*(): bool =
  return not window.windowShouldClose()


proc close*() =
  window.destroyWindow()
  glfwTerminate()


