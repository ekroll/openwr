import nimgl/[opengl, glfw]
import std/options
import openal
import glm

export options, openal, opengl, glfw, glm

type
  Frame* = object
    ## Frame configuration type (parsed from JSON)
    name*: string
    near*: float32
    far*: float32
    fov*: float32
    fps*: int
    res*: array[0 .. 1, int32]

    # Assigned at runtime:
    window*: Option[GLFWWindow]


  Vertex* = object
    position*: array[0 .. 2, float32]
    normal*: array[0 .. 2, float32]
    texcoord*: array[0 .. 1, float32]


  Model* = object
    vertices*: seq[Vertex]
    #indices: seq[vec3]


  DObject* = object
    ## Nim type for assets (parsed from JSON)
    name*: string             # name from JSON
    model*: string            # model from JSON
    spatial*: bool
    pipeline*: string         # shader from JSON
    mass*: float32            # mass from JSON

    # Assigned at runtime:
    id*: Option[int]          # obj id in the continuum
    vai*: Option[uint32]      # vertex array index
    vb*: Option[uint32]       # vertex buffer
    vc*: Option[uint32]       # vertex count
    program*: Option[uint32]  # shader program
    vs*: Option[uint32]       # shader id
    fs*: Option[uint32]       # shader id
    mm*: Option[int32]        # u_model_matrix location
    vm*: Option[int32]        # u_view_matrix location
    pm*: Option[int32]        # u_projection_matrix location
  

  Transform* = object
    coordinate*: glm.Vec[3, float32]
    orientation*: glm.Vec[3, float32]


proc to_model_matrix*(transform: Transform): Mat4[float32] =
  return mat4f(1)
    #[
    .rotate(
      alpha, 
      transform.orientation.x,
      transform.orientation.y,
      transform.orientation.z
    )
    ]#
    .translate(
      transform.coordinate.x,
      transform.coordinate.y,
      transform.coordinate.z
    )