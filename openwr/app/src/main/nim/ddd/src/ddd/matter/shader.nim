import std/strformat
import types as t


proc getCompileStatus(id: uint32) =
  var status: int32
  glGetShaderiv(id, GL_COMPILE_STATUS, status.addr)
  if status == GL_FALSE.ord:
    var length: int32
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, length.addr)
    var message = newSeq[char](length)
    glGetShaderInfoLog(id, length, length.addr, message[0].addr)
    echo "OpenGL Error: The shader compile operation failed. Error message: " &
      cast[string](message[0..^2])
    writeStackTrace()
    quit(QuitFailure)


proc getLinkStatus(id: uint32) =
  var status: int32
  glGetProgramiv(id, GL_LINK_STATUS, status.addr)
  if status == GL_FALSE.ord:
    var length: int32
    glGetProgramiv(id, GL_INFO_LOG_LENGTH, length.addr)
    var message = newSeq[char](length)
    glGetProgramInfoLog(id, length, length.addr, message[0].addr)
    echo "opengl error: The program link operation failed. Error message: " &
      cast[string](message[0..^2])
    writeStackTrace()
    quit(QuitFailure)


proc compileShader(shaderType: GLenum, source: var cstring): uint32 =
  let shader = glCreateShader(shaderType)
  glShaderSource(shader, 1.GLsizei, source.addr, nil)
  glCompileShader(shader)
  getCompileStatus(shader)


proc load*(obj: var t.DObject): t.object=
  var vsrc = readFile(&"{obj.pipeline}/vertex.glsl").cstring
  var fsrc = readFile(&"{obj.pipeline}/fragment.glsl").cstring
  obj.vs = some(compileShader(GL_VERTEX_SHADER, vsrc))
  obj.fs = some(compileShader(GL_FRAGMENT_SHADER, fsrc))

  let program = glCreateProgram()
  obj.program = some(program)
  glAttachShader(program, obj.vs.get)
  glAttachShader(program, obj.fs.get)
  glBindAttribLocation(program, 0, "a_position")
  glBindAttribLocation(program, 1, "a_normal")
  glBindAttribLocation(program, 2, "a_texcoord")
  glLinkProgram(program)
  glValidateProgram(program)
  getLinkStatus(program)

  if obj.spatial:
    obj.mm = some(glGetUniformLocation(program, "u_model_matrix"))
    obj.vm = some(glGetUniformLocation(program, "u_view_matrix"))
    obj.pm = some(glGetUniformLocation(program, "u_perspective_matrix"))


proc dobind*(program: int) =
  glUseProgram(program.GLuint)


proc project*(
  obj: t.DObject,
  frame: t.Frame,
  transform: t.Transform
) =
  ## Update object's transform uniforms
  if obj.mm.isSome:
    var mm = t.to_model_matrix(transform)
    glUniformMatrix4fv(
      obj.mm.get, 
      1.GLsizei,
      false, 
      mm.caddr
    )
 
  if obj.vm.isSome:
    let location = vec3(0.0, 0.0, 0.0)
    let forward = vec3(0.0, 0.0, 1.0)
    let up = vec3(0.0, 1.0, 0.0)
    var vm = lookAt(location, location + forward, up)
    glUniformMatrix4fv(
      obj.vm.get, 
      1.GLsizei,
      false, 
      vm.caddr
    )
  
  if obj.pm.isSome:
    let ratio: float = cast[float](frame.res[0])/cast[float](frame.res[1])
    var pm = perspective(frame.fov, ratio, frame.near, frame.far)
    glUniformMatrix4fv(
      obj.pm.get, 
      1.GLsizei,
      false, 
      pm.caddr
    )

proc unbind*() =
  glUseProgram(0)


proc unload*(vs: int, fs: int) =
  glDeleteShader(vs.GLuint)
  glDeleteShader(fs.GLuint)