import types as t
import res as r

import matter/mesh
import matter/shader

proc create*(obj: var t.DObject) =
  mesh.load(model)
  shader.load(obj)


proc destroy*(obj: t.DObject) =
  mesh.unload(obj.vb.get, vai.vb.get)
  shader.unload(obj.vs)


proc update*(
  obj: t.DObject, 
  status: Frame
) =
  shader.activate(obj.program.get())
  shader.project(obj, status)
  shader.deactivate()
  mesh.draw(obj.vai.get, obj.vc.get)

