import Config


config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]


config :esbuild,
version: "0.14.0",
default: [
  args: ~w(javascript/www.js --bundle --target=es2017 --outdir=../../src/main/res/priv/static/assets --external:/fonts/* --external:/images/*),
  cd: Path.expand("../../app/src/main/", __DIR__),
  env: %{"NODE_PATH" => Path.expand("../../.oof/wws/deps", __DIR__)}
]


config :phoenix, :json_library, Jason
config :wws, ecto_repos: [WWS.Repo]
config :wws, WWS.Repo, priv: "../../app/src/main/res/priv/repo"
config :wws, WWS.Mailer, adapter: Swoosh.Adapters.Local
config :swoosh, :api_client, false


config :wws, WWW.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: WWW.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: WWS.PubSub,
  live_view: [signing_salt: "mvYim6bQ"]


import_config "#{config_env()}.exs"
