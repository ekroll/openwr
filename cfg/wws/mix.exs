defmodule WWS.MixProject do
  use Mix.Project

  def project do
    [
      app: :wws,
      version: "0.1.0",
      elixir: "~> 1.13",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      config_path: "./config.exs",
      build_path: "../../.oof/wws/build/",
      deps_path: "../../.oof/wws/deps/",
      aliases: aliases(),
      deps: deps(),

      # Docs
      name: "World Wide Server",
      source_url: "https://gitlab.com/ekroll/openwr",
      docs: [
        logo: "../../app/src/main/res/priv/static/logo.png",
        extras: ["../../docs/wws.md"]
      ],
    ]
  end


  def application do
    [
      mod: {WWS.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end


  defp elixirc_paths(:test) do
    ["src", Path.expand("../../app/src/test/elixir/support/")]
  end

  defp elixirc_paths(:docs) do
      ["src", Path.expand("../../app/src/main/res/priv/static/docs/web/")]
  end

  defp elixirc_paths(_) do
    [Path.expand("../../app/src/main/elixir/")]
  end


  defp deps do
    [
      {:pbkdf2_elixir, "~> 1.0"},
      {:phoenix, "~> 1.6.6"},
      {:phoenix_ecto, "~> 4.4"},
      {:ecto_sql, "~> 3.6"},
      {:postgrex, "~> 0.15.0"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.17.5"},
      {:floki, ">= 0.30.0", only: :test},
      {:phoenix_live_dashboard, "~> 0.6"},
      {:esbuild, "~> 0.3", runtime: Mix.env() == :dev},
      {:swoosh, "~> 1.3"},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.18"},
      {:jason, "~> 1.2"},
      {:plug_cowboy, "~> 2.5"},
      {:ex_doc, "~> 0.27", only: :dev, runtime: false}
    ]
  end


  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run ../../app/src/main/res/priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      docgen: ["docs --output ../../app/src/main/res/priv/static/docs/wws"],
      "assets.deploy": ["esbuild default --minify", "phx.digest"],
      run: ["docgen", "phx.server"]
    ]
  end
end
