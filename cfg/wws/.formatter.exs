[
  import_deps: [:ecto, :phoenix],
  inputs: ["*.{ex,exs}", "app/src/main/res/priv/*/seeds.exs", "{config,app/src/main/elixir,app/src/test/elixir}/**/*.{ex,exs}"],
  subdirectories: ["app/src/main/res/priv/*/migrations"]
]
