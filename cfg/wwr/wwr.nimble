# Package
version       = "0.1.0"
author        = "Elias Flåte Ekroll"
description   = "World Wide Renderer from OpenWR project"
license       = "GPL-3.0-only"
srcDir        = "../../app/src/main/nim/"
binDir        = "../../.oof/wwr/build/"
bin           = @["wwr"]


# Dependencies
requires "nim >= 1.4.8"
#requires "futhark == 0.5.0" # Waiting until it builds relatively easily
requires "kashae >= 0.1.1"
requires "netty >= 0.2.1"
#requires "ddd"


# Testing
task test, "Runs the test suite":
  exec "nim c -r src/test/nim/test_helper"

task docgen, "Generates documentation":
  exec "nim doc --project --index:on --outdir:../../app/src/main/res/priv/static/docs/ ../../app/src/wwr.nim"
