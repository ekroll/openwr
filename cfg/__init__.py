import subprocess
import importlib
import platform
import zipfile
import pathlib
import urllib.request
import json
import sys
import os


def here():
    return (pathlib
        .Path(__file__)
        .parent)


def die(cmd):
    print(cmd)
    if subprocess.run(cmd).returncode != 0: sys.exit()


def project_root():
    return here().parent


def get_gradlew():
    gradlew = "gradlew"
    if platform.system() == "Windows":
        gradlew = "gradlew.bat"
    
    gradlew = project_root() / gradlew
    return gradlew


def get_path(key):
    paths = here() / "paths.json"
    path = json.load(paths.open("r"))[key]
    return pathlib.Path(path)


def get_endpoint(key):
    endpoints = here() / "endpoints.json"
    return json.load(endpoints.open("r"))[key]


def get_map(key):
    maps = here() / "maps.json"
    return json.load(maps.open("r"))[key]


def makedir(dir):
    if dir is not pathlib.Path:
        dir = pathlib.Path(dir)
    dir.mkdir(parents=True, exist_ok=True)


def prompt(question):
    choice = input(question + " (y/n) = ")
    if choice in ["", "y", "Y"]:
        return True
    else: # lazy
        return False


def setup():
    print("Setting up project...")
    print(here())

    os.chdir(here() / "wws")
    die(["mix", "local.hex"])
    die(["mix", "deps.get"])
    die(["mix", "ecto.setup"])
    die(["mix", "docgen"])

    os.chdir(here() / "wwr")
    die(["choosenim", "stable"])

    os.chdir(here() / get_path("ddd"))
    die(["nimble", "develop"])
    os.chdir(here())

    os.chdir(here() / "wwr")
    die(["nimble", "build"])
    die(["nimble", "docgen"])
    
    os.chdir(project_root())
    pathlib.touch(project_root() / ".oof" / "setup_done")


def proimport(module):
    try:
        return importlib.import_module(module)
    except:
        pip = ["pip", "install", module]
        print(f"pip command = {pip}")
        subprocess.call(pip, shell=True)
        try:
            return importlib.import_module(module)
        except:
            sys.exit()


tqdm = proimport("tqdm")
