## Project structure
I try to make the project structure more or less self explanatory. \
But it has grown, and will probably change. Ill list the essentials.

The repository contains three applications. \
The significant stuff lies under app and subdirs. \
Since Android is the demanding at folder structures, I have made \
every other part of the project obey the standard java structure. \
Under `app/src/main/` there is a folder for every language used... \
and `res` (contains images, sound...)

**Project to language:** \
`wws` -> elixir, javascript, css \
`wwr` -> nim, glsl \
`Diver` -> boilerplate (java)


## OpenWR - World Wide Server:
This binds together all clients on a world instance through a phoenix application. \
Does also contain a web frontend to launch wwr from any coordinate. \
Registered users will appear on the map at their respective locations.
The server code mostly follows 
[standard phoenix structure](https://hexdocs.pm/phoenix/directory_structure.html)
except: \
`lib` -> `app/src/main/elixir` \
`priv` -> `app/src/main/res/priv` \
`test` -> `app/src/test/elixir` \
`config` -> `config/wws` \
`assets` -> `app/src/main/res`


## OpenWR - World Wide Renderer:
This is the client-side part of the OpenWIP stack that connects to wws. \
It is a application written in nim, designed to render the world from API data. \
The world renderer mainly consists of a 3D renderer, a resource loader and the implementation. \
The 3D renderer will probably become a nimble package when it is sort of defined. (and works)

### Notes:
`app/src/main/nim/wwr/ddd` -> wwr's 3D engine (graphics, audio...)  \
`app/src/main/nim/wwr/global` -> directory added to compiler path

### ddd
The 3D renderer does not strictly follow MVC, but you can think this way: \
`src/wwr/ddd/matter.nim` -> **model** (asset loading, mesh handling...) \
`src/wwr/ddd/frame.nim` -> **view** (windowing, rendering...) \
`src/wwr/ddd/continuum.nim` -> **controller** (physics, updating...)


## OpenWR - Diver:
This is a android app that (soon) can act as a VR HMD with wwr. \
Currently there is just a basic UI with a notification. There's \
nothing out of the ordinary excepy for `app.py` that can replace \
the need for android studio when it comes to building and running. \
`python app.py --package no.ekroll.diver --build --install --monitor`